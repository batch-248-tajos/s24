const getCube = 2 ** 3

console.log(`The cube of 2 is ${getCube}`)

const address = ['258 Washington Ave NW', 'California', '90011']
const [street, country, zipCode] = address
console.log(`I live at ${street}, ${country} ${zipCode}`)

const animal = {
	name: 'Lolong',
	type: 'saltwater crocodile',
	weight: '1075 kgs',
	height: '20 ft 3 in.'
}

const {name, type, weight, height} = animal

console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${height}`)

const arrNums = [1, 2, 3, 4, 5] 
arrNums.forEach((item) => console.log(item))

const reduceNumber = arrNums.reduce((a, b) => a+b)
console.log(reduceNumber)

class Dog {
	constructor(name, age, breed) {
		this.name = name
		this.age = age
		this.breed = breed
	}
}

console.log(new Dog('Yumi', 2, 'aspin'))