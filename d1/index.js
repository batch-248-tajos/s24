console.log("Hello B248!");

//ES6 Updates
	//ES6 is one of the latest versions ofwriting JS and in fact is one of the major updates
	
	//let and const
	//are ES6 updates, these are new standards of creating variables
	
	//In JS, hoisting allows you to use functions and variables before they are declared
	//BUT this might cause confusion, because of the confustion that var hoisting can create, it is BEST to AVOID USING VARIABLES BEFORE THEY ARE DECLARED

	console.log(varSample);//undefined
	var varSample = "Hoist me up!!!";

	//if you have used "name" in other parts of the code, you might be surprised at the output we might get. This will likely cause a LOT of bugs in our code. This why "let" and "const" are NECESSARY

	var name = "Camille!"

	if(true){
		var name = "Hi!"
	}

	var name = "Cams"

	console.log("Hi! My Name is " + name);

	//let is block scoped

	let name1 = "Cee"

	if(true){
		let name1 = "Hello";
	}

	console.log(name1); //"Cee"

	//let and const 


	//Exponent Operator

	const firstNum = 8**2;
	console.log(firstNum);//64

	const secondNum = Math.pow(8,2);
	console.log(secondNum);//64

	let eightPowerof3 = 8**3;
	console.log(eightPowerof3);//512

	let string1 = "fun";
	let string2 = "Bootcamp";
	let string3 = "Coding";
	let string4 = "JavaScript";
	let string5 = "Zuitt";
	let string6 = "love";
	let string7 = "Learning";
	let string8 = "I";
	let string9 = "is";
	let string10 = "in";

	/*
		Mini Activity #1 6:39PM
		1. Create new variables called concatSentence1 and concatSentence2
		2. Concatenate and save a resulting string into sentence 1
		3. Concatenate and save a resulting string into sentence 2
			- log both variables in the console and take a screenshot
			- the sentences MUST have spaces and punctuation
	*/

	/*let sentence1 = string8 + " " + string6 + " " + string5 + " " + string3 + " " + string2 + "!";
	let sentence2 = string7 + " " + string4 + " " + string9 + " " + string1 + "!";
	console.log(sentence1);
	console.log(sentence2);*/

	// "", '' - string literals

	//Template Literals
		//allows us to create strings using `` (backticks) and easily embed JS expressions in it
			/*
				it allows us to write strings witout using concatenation operator (+)
				greatly helps with CODE READIBILITY
			*/

	let sentence1 = `${string8} ${string6} ${string5} ${string3} ${string2}!`;
	console.log(sentence1);

	// ${} is a placeholder that is used to embed JS expressions when creating strings using Template Literals

	let name2 = "John"; //Pre-Template literals
	//("") or ('')

	let message = "Hello" + name2 + '! Welcome to programming!';

	//Strings using template literals
	//(``) backticks

	message = `Hello ${name2}! Welcome to programming!`
	console.log(message);


	//Multi-line using Template Literals

	const anotherMessage = `
	${name2} attended a math competition. He won it by solving the problem 8**2 with the solution of ${firstNum}.
	`
	console.log(anotherMessage);

	let dev = {
		name: "Peter",
		lastName: "Parker",
		occupation: "web developer",
		income: 50000,
		expenses: 60000
	};

	console.log(`${dev.name} is a ${dev.occupation}.`);
	console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}.`);

	const interestRate = .1;
	const principal = 1000;

	console.log(`The interest on ${dev.name}'s savings account is ${principal * interestRate}.`)


	//Object Destructuring
	/*
		Allows us to unpack properties of objects into distinct variables

		Syntax
		let/const {propertyNameA, propertyNameB, propertyNameC} = object

	*/


const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
};

//Pre-Object Destructuring

//we can access them using . or []

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);



//Object Destructuring

const {givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

function getFullName ({ givenName, maidenName, familyName}) {
    console.log(`${ givenName } ${ maidenName } ${ familyName }`);
}

getFullName(person);

//copy

let pokemon1 = {

	namePkmn: "Bulbasaur",
	type: "Grass",
	level: 10,
	moves: ["Razor Leaf","Tackle","Leech Seed"]

}

let {level,type,namePkmn,moves,personality} = pokemon1;

console.log(level);
console.log(type);
console.log(namePkmn);
console.log(moves);
console.log(personality);//undefined

let pokemon2 = {

	namePkmn: "Charmander",
	type: "Fire",
	level: 11,
	moves: ["Ember","Scratch"]

}

//{propertyName: newVariable}
const {namePkmn: nameVar} = pokemon2;
console.log(nameVar);

console.log(`${nameVar} goes char char!`)


//Array Destructuring
	//allows us to unpack elements in arrays into distinct variables
	//allows us to name array elements with variables instead of using index numbers

	/*
		let/const [variableName, variableName, variableName] = array;
	
	*/

	const fullName = ["Juan","Dela","Cruz"];

	//pre-array destructuring
	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);

	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you!`)


	//array destructuring
	const [firstName, middleName, lastName] = fullName;
	console.log(firstName);
	console.log(middleName);
	console.log(lastName);

	console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you again!`)

	let oneDirection = ["Harry","Zayn","Niall","Louis","Liam"];

	let [singer1, , singer3, singer4, singer5] = oneDirection;
	console.log(singer3);//Niall
	

	//Arrow Function
	/*

		an alternative way of writing functions in JS
		Syntax:
		let/const variableName = (parameterA, parameterB, parameterC) => {
			console.log;
		}
	*/

	//Traditional Function
	function displayMsg(){
		console.log("Hello World!");
	}

	displayMsg();

	//Arrow Function

	const hello = () => {
		console.log("Hello from Arrow Function!")
	}

	hello();

	//Arrow Function with Parameters

	const greet = (friend) => {
		console.log(`Hi ${friend}`);
	}

	greet("Zayn");

	//Arrow vs Traditional Function

	//Implicit Return - allows us to return a value form an arrow function without the use of return keyword
	//traditional addNum() function

	function addNum(num1,num2){
		let result = num1 + num2;
		return result
	}
	let sum = addNum(5,10);
	console.log(sum);

	//arrow function

	let subNum = (num1,num2) => num1 - num2;

	let difference = subNum(10,5);
	console.log(difference);

	/*
		MiniA (3mins)

		create a addition, subtraction, multiplication and division arrow function
		use 7 and 5 as your arguments
		log the toal in your console

	*/
	let addNo = (num1,num2) => num1 + num2;
	let sum1 = addNo(7,5);
	console.log(sum1);

	let subNo = (num1,num2) => num1 - num2;
	let difference1 = subNo(7,5);
	console.log(difference1);

	let multNo = (num1,num2) => num1 * num2;
	let product = multNo(7,5);
	console.log(product);

	let divideNo = (num1,num2) => num1 / num2;
	let quotient = divideNo(7,5);
	console.log(quotient);

	//Arrow Functions with loops

	//pre-arrow function

	const bears = ["Morgan","Amy","Lulu"];

	bears.forEach(function(bear){
		console.log(`${bear} is my bestfriend!`)
	})

	//arrow function

	bears.forEach((bear)=>{
		console.log(`${bear} is my best buddy!`)
	})


	//Default Function Argument Value
	//provide a default argument value if none is provided when the function is invoked

	const messageForYou = (name = 'User') =>{
		return `Good evening, ${name}. Hope you are okay =>`;
	};

	console.log(messageForYou());
	console.log(messageForYou("Zayn"));


	//Class-Based Object Blueprints

	/*
		Allows creation/instantiation of objects using classes as blueprints

		Syntax

		class Classname {
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}

	*/

	class Car {
		constructor(brand,name,year){
			this.brand = brand;
			this.name = name;
			this.year = year;

		}
	}

	//Instantiating an object
	/*
		the "new" operator creates/instantiates a new object with the given arguments as the value of its properties

	*/

	//let/const variableName = new className();
	
	const myCar = new Car();
	console.log(myCar);//undefined

	myCar.brand = "Ford";
	myCar.name = "Everest";
	myCar.year = "1996";

	console.log(myCar);

	const myNewCar = new Car("Toyota","Vios",2000);
	console.log(myNewCar);

	/*
		MiniA (3-5min)
		create a "Chracter" Class Constructor

		name:
		role:
		strength:
		weakness:

		create 2 new characters out of the class constructor and save it in their respective variables
		-log the variables, send a ss

	*/

	class Character {
		constructor(name, role, strength, weakness){
			this.name = name;
			this.role = role;
			this.strength = strength;
			this.weakness = weakness;
			this.introduce = () =>{
				console.log(`Hi! I am ${this.name}!`)
			}
		}
	}

	const starlightAnya = new Character("Starlight Anya", "Mage of Cuteness","Mama and Papa","Peanuts");


	console.log(starlightAnya);
	starlightAnya.introduce();